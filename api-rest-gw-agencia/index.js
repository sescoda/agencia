    'use strict'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const port = process.env.PORT || 3100;

const https = require('https');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const fs = require('fs');
const helmet = require('helmet');
const passService = require('../auth/services/pass.service');
const tokenService = require('../auth/services/token.service');
const { token } = require('morgan');
const moment = require('moment');

const URL_VEHICULOS = "https://localhost:3000/api/Vehiculos";
const URL_VUELOS = "https://localhost:3001/api/Vuelos";
const URL_HOTELES = "https://localhost:3002/api/Hoteles";
const URL_USUARIOS = "https://localhost:3010/api/Usuarios";
const URL_TRANSACCIONES = "https://localhost:3004/api/Transacciones";

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}

const usuario = {
    email: '',
    name:'',
    password: '',

};


const app = express();
app.use(helmet());

//Añadir middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());



function auth (req, res, next ) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];

    tokenService.decodificarToken(miToken)
    .then( userID => {
   
        req.user = {
            userId: userID,
            token: miToken
        }  
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Acceso no autorizado'
            });
        
            return next(new Error("Acceso no autorizado"));
        });
    
    
}

//RUTAS Y CONTROLADORES

//RESERVAS

app.post('/api/Transacciones/reservarVehiculo/:id', auth, (req, res, next) => {

    const queId = req.params.id;
    
    const elemento = {
        Primer_Paso: 'Comienza la transaccion',
        InicioTiempo: moment().unix(),
        id: queId
    }
    const queToken = req.headers.authorization.split(" ")[1];
    let queURL = `${URL_TRANSACCIONES}/comenzarTransaccion`;

    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        
        
            queURL = `${URL_TRANSACCIONES}/prereservarVehiculo`;
            fetch(queURL, {
                        method: 'PUT',
                        body: JSON.stringify(mijson.result),
                        headers: {'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                                }
                        })
            .then(res => res.json())
            .then(mijson => {
                if(mijson.result.Tercer_paso == 'Fin de la Transaccion')
                {
                    res.status(400).json(
                        {
                            result: 'No existe el elemento a reservar, compruebe el id'
                        }
                    )
                }
                else{
                    queURL = `${URL_TRANSACCIONES}/pagarVehiculo`;
        
                    fetch(queURL, {
                                    method: 'PUT',
                                    body: JSON.stringify(mijson.result),
                                    headers: {'Content-Type': 'application/json',
                                            'Authorization': `Bearer ${queToken}`                                   
                                            }
        
                                })
                    .then(res => res.json())
                    .then (mijson => {
        
                        if(mijson.result.Quinto_paso == 'Coche Reservado')
                        {
                            res.status(200).json(
                                {
                                    result: 'Reserva realizada correctamente'
                                }
                            )
        
        
                        }
                        else
                        {
                            res.status(400).json(
                                {
                                    result: 'Error al realizar la reserva'
                                }
                            )
                        }
                    })
                }
            })
        

    })
    


});

app.post('/api/Transacciones/reservarHotel/:id', auth, (req, res, next) => {


    const queId = req.params.id;
    
    const elemento = {
        Primer_Paso: 'Comienza la transaccion',
        InicioTiempo: moment().unix(),
        id: queId
    }
    const queToken = req.headers.authorization.split(" ")[1];
    let queURL = `${URL_TRANSACCIONES}/comenzarTransaccion`;

    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        queURL = `${URL_TRANSACCIONES}/prereservarHotel`;
        fetch(queURL, {
                    method: 'PUT',
                    body: JSON.stringify(mijson.result),
                    headers: {'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}`                                   
                            }
                    })
        .then(res => res.json())
        .then(mijson => {
            if(mijson.result.Tercer_paso == 'Fin de la Transaccion')
                {
                    res.status(400).json(
                        {
                            result: 'No existe el elemento a reservar, compruebe el id'
                        }
                    )
                }
            else{
                queURL = `${URL_TRANSACCIONES}/pagarHotel`;

                fetch(queURL, {
                                method: 'PUT',
                                body: JSON.stringify(mijson.result),
                                headers: {'Content-Type': 'application/json',
                                        'Authorization': `Bearer ${queToken}`                                   
                                        }

                            })
                .then(res => res.json())
                .then (mijson => {

                    if(mijson.result.Quinto_paso == 'Hotel Reservado')
                    {
                        res.status(200).json(
                            {
                                result: 'Reserva realizada correctamente'
                            }
                        )


                    }
                    else
                    {
                        res.status(400).json(
                            {
                                result: 'Error al realizar la reserva'
                            }
                        )
                    }
                })
            }
        })
    })
    
});

app.post('/api/Transacciones/reservarVuelo/:id', auth, (req, res, next) => {

    
    const queId = req.params.id;
    
    const elemento = {
        Primer_Paso: 'Comienza la transaccion',
        InicioTiempo: moment().unix(),
        id: queId
    }
    const queToken = req.headers.authorization.split(" ")[1];
    let queURL = `${URL_TRANSACCIONES}/comenzarTransaccion`;

    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        queURL = `${URL_TRANSACCIONES}/prereservarVuelo`;
        fetch(queURL, {
                    method: 'PUT',
                    body: JSON.stringify(mijson.result),
                    headers: {'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}`                                   
                            }
                    })
        .then(res => res.json())
        .then(mijson => {
            if(mijson.result.Tercer_paso == 'Fin de la Transaccion')
                {
                    res.status(400).json(
                        {
                            result: 'No existe el elemento a reservar, compruebe el id'
                        }
                    )
                }
            else{
                queURL = `${URL_TRANSACCIONES}/pagarVuelo`;

                fetch(queURL, {
                                method: 'PUT',
                                body: JSON.stringify(mijson.result),
                                headers: {'Content-Type': 'application/json',
                                        'Authorization': `Bearer ${queToken}`                                   
                                        }

                            })
                .then(res => res.json())
                .then (mijson => {

                    if(mijson.result.Quinto_paso == 'Vuelo Reservado')
                    {
                        res.status(200).json(
                            {
                                result: 'Reserva realizada correctamente'
                            }
                        )


                    }
                    else
                    {
                        res.status(400).json(
                            {
                                result: 'Error al realizar la reserva'
                            }
                        )
                    }
                })
            }
        })
    })
    
});

//VEHICULOS
app.get('/api/Vehiculos', (req, res, next) => {
    const queMarca = req.query.Marca;
    const queColor = req.query.Color;
    
    let queURL;
    if(JSON.stringify(req.query) == '{}'){
        queURL = `${URL_VEHICULOS}`;
    }
    else{
        queURL = `${URL_VEHICULOS}?Marca=${queMarca}&Color=${queColor}`;
    }
    
    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vehiculos',
            elementos: mijson.Vehiculos
        });
    });
        
});

app.get('/api/Vehiculos/:id', (req, res, next) => {

    const idElemento = req.params.id;
    const queURL = `${URL_VEHICULOS}/${idElemento}`;

    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vehiculos',
            elemento: mijson.elementos
        });
    });
    
});

app.post('/api/Vehiculos', auth, (req, res, next) => {


    const elemento = req.body;

    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_VEHICULOS}`;

    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vehiculos',
            elemento: mijson.elementos
        });
    });
});


app.put('/api/Vehiculos/:id', auth, (req, res, next) => {

    const elemento = req.body;
    const idElemento = req.params.id;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_VEHICULOS}/${idElemento}`;

    fetch(queURL, {
                    method: 'PUT',
                    body: JSON.stringify(elemento),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( resp => resp.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vehiculos',
            elemento: idElemento
        });
    });
});

app.delete('/api/Vehiculos/:id', auth, (req, res, next) => {
    
    const idElemento = req.params.id;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_VEHICULOS}/${idElemento}`;

    fetch(queURL, {
                    method: 'DELETE',
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vehiculos',
            elemento: idElemento
        });
    });

});



//VUELOS
app.get('/api/Vuelos', (req, res, next) => {

    const queOrigen = req.query.Marca;
    const queDestino = req.query.Color;
    let queURL;
    if(JSON.stringify(req.query) == '{}'){
        queURL = `${URL_VUELOS}`;
    }
    else{
        queURL = `${URL_VUELOS}?Origen=${queOrigen}&Destino=${queDestino}`;
    }


    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vuelos',
            elementos: mijson.Vuelos
        });
    });
        
});

app.get('/api/Vuelos/:id', (req, res, next) => {

    const idElemento = req.params.id;
    const queURL = `${URL_VUELOS}/${idElemento}`;

    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vuelos',
            elemento: mijson.elementos
        });
    });
    
});

app.post('/api/Vuelos', auth, (req, res, next) => {


    const elemento = req.body;

    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_VUELOS}`;

    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vuelos',
            elemento: mijson.elemento
        });
    });
});


app.put('/api/Vuelos/:id', auth, (req, res, next) => {

    const elemento = req.body;
    const idElemento = req.params.id;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_VUELOS}/${idElemento}`;

    fetch(queURL, {
                    method: 'PUT',
                    body: JSON.stringify(elemento),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( resp => resp.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vuelos',
            elemento: mijson.elemento
        });
    });
});

app.delete('/api/Vuelos/:id', auth, (req, res, next) => {
    
    const idElemento = req.params.id;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_VUELOS}/${idElemento}`;

    fetch(queURL, {
                    method: 'DELETE',
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Vuelos',
        
        });
    });

});




//HOTELES

app.get('/api/Hoteles', (req, res, next) => {

    const queCiudad = req.query.Ciudad;
    const queFechaEntrada = req.query.FechaEntrada;
    let queURL;
    if(JSON.stringify(req.query) == '{}'){
        queURL = `${URL_HOTELES}`;
    }
    else{
        queURL = `${URL_HOTELES}?Ciudada=${queCiudad}&FechaEntrada=${queFechaEntrada}`;
    }


    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Hoteles',
            elementos: mijson.Hoteles
        });
    });
        
});

app.get('/api/Hoteles/:id', (req, res, next) => {

    const idElemento = req.params.id;
    const queURL = `${URL_HOTELES}/${idElemento}`;

    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Hoteles',
            elemento: mijson.elementos
        });
    });
    
});

app.post('/api/Hoteles', auth, (req, res, next) => {


    const elemento = req.body;

    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_HOTELES}`;

    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Hoteles',
            elemento: mijson.elemento
        });
    });
});


app.put('/api/Hoteles/:id', auth, (req, res, next) => {

    const elemento = req.body;
    const idElemento = req.params.id;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_HOTELES}/${idElemento}`;

    fetch(queURL, {
                    method: 'PUT',
                    body: JSON.stringify(elemento),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( resp => resp.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Hoteles',
            elemento: mijson.elemento
        });
    });
});

app.delete('/api/Hoteles/:id', auth, (req, res, next) => {
    
    const idElemento = req.params.id;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_HOTELES}/${idElemento}`;

    fetch(queURL, {
                    method: 'DELETE',
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Hoteles',
        
        });
    });

});

//REGISTRO


app.get('/api/Hoteles/:id', (req, res, next) => {

    const idElemento = req.params.id;
    const queURL = `${URL_HOTELES}/${idElemento}`;

    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'Hoteles',
            elemento: mijson.elementos
        });
    });
    
});

app.post('/api/Usuarios',  (req, res, next) => {


    //const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_USUARIOS}`;

    let elemento = 
    {
       email: req.body.email,
       name: req.body.name,
       password: req.body.password,
       signUp: moment().unix(),
       lastLogin: moment().unix()
    };
    usuario.email = req.body.email;
    usuario.name = req.body.name; 
    usuario.password = elemento.password;
    passService.encriptaPassword(usuario.password)
    .then(hash => {
        elemento.password = hash;
        elemento.token = tokenService.crearToken(usuario);
        fetch(queURL, {
                        method: 'POST',
                        body: JSON.stringify(elemento),
                        headers: {'Content-Type': 'application/json'}
                        })
        .then( res => res.json() )
        .then( mijson => {
            res.json({
                result: 'OK',
                elemento: mijson.elemento,
    
            });
            
        });
    });

});


app.put('/api/Usuarios', (req, res, next) => {

    const queURL = `${URL_USUARIOS}?email=${req.body.email}`;

    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {

        if(mijson.Elemento == null)
        {
            res.status(400).json({
                result: 'No existe el usuario'
            })
        }
        else
        {
            let elemento = 
                {
                    email: mijson.Elemento.email,
                    password: mijson.Elemento.password,
                    token: mijson.Elemento.token,
                    lastLogin: moment().unix()
                }
              

            passService.comparaPassword(req.body.password, elemento.password)
            .then(isOk => {
                
                if(isOk) {
                    fetch(queURL, {
                        method: 'PUT',
                        body: JSON.stringify(elemento),
                        headers: {
                            'Content-Type': 'application/json',
                                }
        
                        })
                    .then( resp => resp.json() )
                    .then( mijson => {
                                res.json({
                                result: 'OK',
                                token: elemento.token
                                });
                    });
        
                }
                else{
                    res.status(400).json({
                        result: 'Error en el incio de sesion'
                    })
                }
            });
        }
    });

});


//Creamos el Servidor
https.createServer(opciones, app).listen(port, () => {
    console.log(`API GW RESTful CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});






