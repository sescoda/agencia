'use strict'

const port = process.env.PORT || 3001;
const URL_DB = "mongodb+srv://SD:passwordSD@cluster0.ehudi.mongodb.net/Vuelos?retryWrites=true&w=majority";


const https = require('https');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const fs = require('fs');
const helmet = require('helmet');
const mongoose = require('mongoose');

const tokenService = require('../auth/services/token.service');

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}


const app = express();
app.use(helmet());

var db = mongojs(URL_DB); //Enlazando con la BD
var id = mongojs.ObjectID; //funcion que convierte num en objeto

const coleccion = db.Vuelos;

//Añadir middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.param("colecciones", (req, res, next, coleccion) => {
    console.log('param /api/:colecciones');
    console.log('coleccion: ', coleccion);

    req.collection = db.collection(coleccion); //Puntero a funicon que apunta a la BD
    return next();
});

//Autorizacion
function auth (req, res, next ) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];

    tokenService.decodificarToken(miToken)
    .then( userID => {
        const id = userID;
        req.user = {
        userId: userID,
        token: miToken
        }  
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Acceso no autorizado'
            });
        
            return next(new Error("Acceso no autorizado"));
            //return next(new Error("Token no valido"));
        });
}

// Implementar las rutas de nuestra API
app.get('/api', (req, res, next) => {

    db.getCollectionNames((err, colecciones) => {
        if(err) return next(err);
        console.log(colecciones);
        res.json({
            result: 'OK',
            colecciones: colecciones
        });
    });
});

app.get('/api/Vuelos', (req, res, next) => {

    if(JSON.stringify(req.query) == '{}'){
        coleccion.find({estado: 'libre'},(err, elemento) => {
            if (err) return next(err);

            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: 'Vuelos',
                Vuelos: elemento
            });
        });
    }
    else{
        const queDestino = req.query.Destino;
        const queOrigen = req.query.Origen;


        coleccion.findOne({Origen: queOrigen, Destino: queDestino, Estado: 'libre'},(err, elemento) => {
            if (err) return next(err);

            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: 'Vuelos',
                Vuelos: elemento
            });
        });
    }
});

app.get('/api/Vuelos/:id', (req, res, next) => {

    const idElemento = req.params.id;
    coleccion.findOne({_id: id(idElemento)},(err, elemento) => {
        if (err) return next(err);

        console.log(elemento);
        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elementos: elemento
        });
    });
});

app.post('/api/Vuelos', auth, (req, res, next) => {


    const elemento = req.body;

    coleccion.save(elemento, (err, elementoGuardado) =>{
        if(err) return next(err);

        res.json({
            result: 'OK',
            coleccion: req.params.colleciones,
            elemento: elementoGuardado
        });
    });
});


app.put('/api/Vuelos/:id', auth, (req, res, next) => {

    let elementoId = req.params.id;
    let elementoNuevo = req.body;

    coleccion.update(
        {_id: id(elementoId)}, 
        {$set: elementoNuevo}, 
        {safe: true, multi: false}, 
        (err, elementoModif) => {
            if(err) return next(err);

            res.json({
                result: 'OK',
                coleccion: req.params.colecciones,
                id: elementoId,
                resultado: elementoModif
            });
    });
});

app.delete('/api/Vuelos/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;

    coleccion.remove({_id: id(elementoId)}, (err, resultado) => {
        if(err) return next(err);

        res.json({
            result: 'OK',
            elementos: resultado
        });
    });
});


https.createServer(opciones, app).listen(port, () => {
    console.log(`API RESTful CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});

