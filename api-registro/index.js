'use strict'

const port = process.env.PORT || 3010;
const URL_DB = "mongodb+srv://SD:passwordSD@cluster0.ehudi.mongodb.net/Agencia?retryWrites=true&w=majority";


const https = require('https');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const fs = require('fs');
const helmet = require('helmet');

const passService = require('../auth/services/pass.service');
const tokenService = require('../auth/services/token.service');
const { response } = require('express');

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}

const usuario = {
    email: '',
    name:'',
    password: '',
    signUp: '',
    lastLogin: '',

};

const app = express();
app.use(helmet());

var db = mongojs(URL_DB); //Enlazando con la BD
var id = mongojs.ObjectID; //funcion que convierte num en objeto
const coleccion = db.Usuarios;

//Añadir middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


// Implementar las rutas de nuestra API

app.get('/api/Usuarios', (req, res, next) => {

    const queEmail = req.query.email;

        coleccion.findOne({email: queEmail},(err, elemento) => {
            if (err) return next(err);

            res.json({
                Elemento: elemento
            });
        });
});

app.post('/api/Usuarios', (req, res, next) => {

    const elemento = req.body;
    usuario.email = req.body.email;
    usuario.name = req.body.name; 
    usuario.password = elemento.password;


    passService.encriptaPassword(usuario.password)
    .then(hash => {
        elemento.password = hash;
    });
    
    coleccion.save(elemento, (err, elementoGuardado) =>{
        if(err) return next(err);

        res.json({
            result: 'OK',
            coleccion: req.params.colleciones,
            elemento: elementoGuardado,
            token: tokenService.crearToken(usuario.password)
        });
    });
});


app.put('/api/Usuarios', (req, res, next) => {

    let elementoNuevo = req.body;

    coleccion.update(
            {email: elementoNuevo.email}, 
            {$set: elementoNuevo}, 
            {safe: true, multi: false}, 
            (err, elementoModif) => {
                if(err) return next(err);
    
                res.json({
                    result: 'OK',
                    coleccion: req.params.colecciones,
                    resultado: elementoModif
                });
        });
        
});



https.createServer(opciones, app).listen(port, () => {
    console.log(`API Registro ejecutandose en https://localhost:${port}/api/{colecciones}`);
});