'use strict'

const port = process.env.PORT || 3005;



const https = require('https');
const express = require('express');
const logger = require('morgan');
const fs = require('fs');
const helmet = require('helmet');

//const tokenService = require('../auth/services/token.service');

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}


const app = express();
app.use(helmet());



//Añadir middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


app.get('/api/Pagos', (req, res, next) => {

        let numero = Math.floor(Math.random() * (10));
        if(numero < 8)
        {
            res.json({
                result: 'Pago realizado correctamente'
            }); 
        }
        else{
            res.json({
                result: 'Error al realizar el pago'
            });
        }
         
});




https.createServer(opciones, app).listen(port, () => {
    console.log(`API RESTful CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});


