'use strict'

const passService = require('./services/pass.service');
const moment = require('moment');

//Datos simulados
const miPass = "1234";
const badPass = "6789";
const usuario = {
    _id: "1231412313",
    email: 'sergio15600@gmail.com',
    displayName:'sescoda',
    password: miPass,
    signupDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//Encriptar password
passService.encriptaPassword(usuario.password)
.then (hash => {
    usuario.password = hash;
    console.log(usuario);

    //Verificamos pass
    passService.comparaPassword(miPass, usuario.password)
    .then(isOk => {
        if(isOk) {
            console.log('p1: El password es correcto');

        }
        else{
            //hacer un response 400
            console.log('p1: El password es incorecto');
        }
    });

    //Verificamos pass con un errpr
    passService.comparaPassword(badPass, usuario.password)
    .then(isOk => {
        if(isOk) {
            console.log('p2: El password es correcto');

        }
        else{
            console.log('p2: El password es incorrecto');
        }
    });
});