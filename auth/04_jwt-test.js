'use strict'

const tokenService = require('./services/token.service');
const moment = require('moment');

//Simular datos
const miPass = "1234";
const usuario = {
    _id: "1231412313",
    email: 'sergio15600@gmail.com',
    displayName:'sescoda',
    password: miPass,
    signupDate: moment().unix(),
    lastLogin: moment().unix()
};
console.log(usuario);
// Crear un token
const token = tokenService.crearToken(usuario);

console.log( token);

//Decodificar el token
tokenService.decodificarToken(token)
    .then( userID => {
        return console.log(`ID1: ${userID}`)
    })
    .catch( err => console.log({Error1: err})
    );

