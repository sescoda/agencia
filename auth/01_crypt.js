'use strict'

const bcrypt = require('bcrypt');

const miPass = "miContraseña";
const badPass = "otraContraseña";

//Creamos salt
bcrypt.genSalt(10, (err, salt) => {
    console.log(`Salt1: ${salt}`);

    bcrypt.hash( miPass, salt, (err, hash) => {
        if(err) console.log(err);
        else console.log(`Hash1: ${hash}`);
    });
});

bcrypt.hash(miPass, 10, (err, hash) => {
    if(err) console.log(err);
    else {
        console.log(`Hash2 (password codificado): ${hash}`);
        
        bcrypt.compare( miPass, hash, (err, result) => {
            console.log(`Result2.1: ${result}`);
        });

        bcrypt.compare( badPass, hash, (err, result) => {
            console.log(`Result2.2: ${result}`);
        });
    }
});