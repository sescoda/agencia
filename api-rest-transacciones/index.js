    'use strict'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const port = process.env.PORT || 3004;
const URL_DB = "mongodb+srv://SD:passwordSD@cluster0.ehudi.mongodb.net/Transacciones?retryWrites=true&w=majority";

const https = require('https');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const fs = require('fs');
const helmet = require('helmet');
const passService = require('../auth/services/pass.service');
const tokenService = require('../auth/services/token.service');
const { token } = require('morgan');
const mongojs = require('mongojs');


const URL_VEHICULOS = "https://localhost:3000/api/Vehiculos";
const URL_VUELOS = "https://localhost:3001/api/Vuelos";
const URL_HOTELES = "https://localhost:3002/api/Hoteles";
const URL_PAGOS = "https://localhost:3005/api/Pagos";

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}


const app = express();
app.use(helmet());

var db = mongojs(URL_DB); //Enlazando con la BD
var id = mongojs.ObjectID; //funcion que convierte num en objeto
const coleccion = db.Transacciones;

//Añadir middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


app.param("colecciones", (req, res, next, coleccion) => {
    console.log('param /api/:coleccion');
    console.log('coleccion: ', coleccion);

    req.collection = db.collection(coleccion); //Puntero a funicon que apunta a la BD
    return next();
});


function auth (req, res, next ) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];

    tokenService.decodificarToken(miToken)
    .then( userID => {
   
        req.user = {
            userId: userID,
            token: miToken
        }  
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Acceso no autorizado'
            });
        
            return next(new Error("Acceso no autorizado"));
        });
    
    
}

//RUTAS Y CONTROLADORES
//RESERVA


app.get('api/Transacciones', (req,res,next) => {
    coleccion.find((err, coleccion) => {
        if (err) return next(err);

        console.log(coleccion);
        res.json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elementos: coleccion
        });
    });
});

app.post('/api/Transacciones/comenzarTransaccion', auth, (req, res, next) => {
    let elemento = 
    { Primer_Paso: req.body.Primer_Paso,
      InicioTiempo: req.body.InicioTiempo
    };

    //GUARDAMOS EL INICIO DE LA TRANSACCION
    coleccion.save(elemento, (err, elementoGuardado) =>{
        if(err) return next(err);
        
        elemento = req.body
        res.json({
            result: elemento

        });
    });

});

app.put('/api/Transacciones/prereservarVehiculo', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = req.body.id;
    let reservado = { estado : 'prereservado'};
    let URLVehiculo = `${URL_VEHICULOS}/${idElemento}`;
    const queToken = req.headers.authorization.split(" ")[1];

    fetch(URLVehiculo, {
                            method: 'PUT',
                            body: JSON.stringify(reservado),
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                            }

                        })
    .then( resp => resp.json() )
    .then(mijson =>
        {
            if(mijson.resultado.nModified != 0){
                elemento = {
                    Segundo_paso:'El vehiculo ' + idElemento +' esta en prereserva',
                    Tercer_paso: 'Pendiente de pago',
                }
            
                coleccion.update(
                    {InicioTiempo: req.body.InicioTiempo}, 
                    {$set: elemento}, 
                    {safe: true, multi: false}, 
                    (err, elementoModif) => {
                        if(err) return next(err);
                        
                        elemento = 
                        {
                            Primer_Paso: req.body.Primer_Paso,
                            InicioTiempo: req.body.InicioTiempo,
                            Segundo_paso:'El vehiculo ' + idElemento +' esta en prereserva',
                            Tercer_paso: 'Pendiente de pago',
                        };
                        res.json({
                            result: elemento
                        });
                });
            }
            else
            {
                elemento = {
                    Segundo_paso: 'No existe el elemento con id: ' + idElemento,
                    Tercer_paso: 'Fin de la Transaccion'
                }

                coleccion.update(
                    {InicioTiempo: req.body.InicioTiempo}, 
                    {$set: elemento}, 
                    {safe: true, multi: false}, 
                    (err, elementoModif) => {
                        if(err) return next(err);

                        elemento = 
                        {
                            Primer_Paso: req.body.Primer_Paso,
                            InicioTiempo: req.body.InicioTiempo,
                            Segundo_paso:'No existe el elemento con id: ' + idElemento,
                            Tercer_paso: 'Fin de la Transaccion',
                        };
                        res.json({
                            result: elemento
                        });
                });
            }
        })
    

});

app.put('/api/Transacciones/pagarVehiculo', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = JSON.stringify(req.body.Segundo_paso).split(" ")[2];

    let reservado = { estado : 'prereservado'};
    let URLVehiculo = `${URL_VEHICULOS}/${idElemento}`;
    let URLPago = `${URL_PAGOS}`;
    const queToken = req.headers.authorization.split(" ")[1];
    

//PAGAMOS LA RESERVA
    fetch(URLPago)
    .then( resp => resp.json() )
    .then(mijson =>
    {

        if(mijson.result == 'Pago realizado correctamente')
        {

            reservado = { estado: 'reservado'};
            fetch(URLVehiculo ,{ 
                        method: 'PUT',
                        body: JSON.stringify(reservado),
                        headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                        }
    
            })
            .then( resp => resp.json() )

            
            elemento = {
                Cuarto_paso: 'Pago realizado',
                Quinto_paso: 'Coche Reservado'
            };

            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);
                    
                    elemento = {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:req.body.Segundo_paso,
                        Tercer_paso: req.body.Tercer_paso,
                        Cuarto_paso: 'Pago realizado',
                        Quinto_paso: 'Coche Reservado'
                    }

                    res.json({
                        result: elemento
                    });
            });
        }
        else
        {
            reservado = { estado: 'libre'};

            fetch(URLVehiculo ,{ 
                        method: 'PUT',
                        body: JSON.stringify(reservado),
                        headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                        }
    
            })
            .then( resp => resp.json() )

            
            elemento = {
                Cuarto_paso: 'Error en el pago',
                Quinto_paso: 'Coche Liberado'
            };

            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);
                    
                    elemento = {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:req.body.Segundo_paso,
                        Tercer_paso: req.body.Tercer_paso,
                        Cuarto_paso: 'Error en el pago',
                        Quinto_paso: 'Coche Liberado'
                    }

                    res.json({
                        result: elemento
                    });
            });
        }
    } )
})



//HOTELES

app.put('/api/Transacciones/prereservarHotel', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = req.body.id;
    let reservado = { estado : 'prereservado'};
    let URLHotel = `${URL_HOTELES}/${idElemento}`;
    const queToken = req.headers.authorization.split(" ")[1];

    fetch(URLHotel, {
                            method: 'PUT',
                            body: JSON.stringify(reservado),
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                            }

                        })
    .then( resp => resp.json() )
    .then(mijson => {
        if(mijson.resultado.nModified != 0){
            elemento = {
                Segundo_paso:'El hotel ' + idElemento +' esta en prereserva',
                Tercer_paso: 'Pendiente de pago',
            }
        
            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);
                    
                    elemento = 
                    {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:'El hotel ' + idElemento +' esta en prereserva',
                        Tercer_paso: 'Pendiente de pago',
                    };
                    res.json({
                        result: elemento
                    });
            });
        }
        else{
            elemento = {
                Segundo_paso: 'No existe el elemento con id: ' + idElemento,
                Tercer_paso: 'Fin de la Transaccion'
            }

            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);

                    elemento = 
                    {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:'No existe el elemento con id: ' + idElemento,
                        Tercer_paso: 'Fin de la Transaccion',
                    };
                    res.json({
                        result: elemento
                    });
            });
        }
    })
    

});

app.put('/api/Transacciones/pagarHotel', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = JSON.stringify(req.body.Segundo_paso).split(" ")[2];

    let reservado = { estado : 'prereservado'};
    let URLHotel = `${URL_HOTELES}/${idElemento}`;
    let URLPago = `${URL_PAGOS}`;
    const queToken = req.headers.authorization.split(" ")[1];
    

//PAGAMOS LA RESERVA
    fetch(URLPago)
    .then( resp => resp.json() )
    .then(mijson =>
    {

        if(mijson.result == 'Pago realizado correctamente')
        {
            reservado = { estado: 'reservado'};
            fetch(URLHotel ,{ 
                        method: 'PUT',
                        body: JSON.stringify(reservado),
                        headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                        }
    
            })
            .then( resp => resp.json() )

            
            elemento = {
                Cuarto_paso: 'Pago realizado',
                Quinto_paso: 'Hotel Reservado'
            };

            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);
                    
                    elemento = {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:req.body.Segundo_paso,
                        Tercer_paso: req.body.Tercer_paso,
                        Cuarto_paso: 'Pago realizado',
                        Quinto_paso: 'Hotel Reservado'
                    }

                    res.json({
                        result: elemento
                    });
            });
        }
        else
        {
            reservado = { estado: 'libre'};

            fetch(URLHotel ,{ 
                        method: 'PUT',
                        body: JSON.stringify(reservado),
                        headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                        }
    
            })
            .then( resp => resp.json() )

            
            elemento = {
                Cuarto_paso: 'Error en el pago',
                Quinto_paso: 'Hotel Liberado'
            };

            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);
                    
                    elemento = {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:req.body.Segundo_paso,
                        Tercer_paso: req.body.Tercer_paso,
                        Cuarto_paso: 'Error en el pago',
                        Quinto_paso: 'Hotel Liberado'
                    }

                    res.json({
                        result: elemento
                    });
            });
        }
    } )
})


/// VUELOS

app.put('/api/Transacciones/prereservarVuelo', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = req.body.id;
    let reservado = { estado : 'prereservado'};
    let URLVuelo = `${URL_VUELOS}/${idElemento}`;
    const queToken = req.headers.authorization.split(" ")[1];

    fetch(URLVuelo, {
                            method: 'PUT',
                            body: JSON.stringify(reservado),
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                            }

                        })
    .then( resp => resp.json() )
    .then(mijson => {
        if(mijson.resultado.nModified != 0)
        {
            elemento = {
                Segundo_paso:'El vuelo ' + idElemento +' esta en prereserva',
                Tercer_paso: 'Pendiente de pago',
            }
        
            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);
                    
                    elemento = 
                    {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:'El vuelo ' + idElemento +' esta en prereserva',
                        Tercer_paso: 'Pendiente de pago',
                    };
                    res.json({
                        result: elemento
                    });
            });
        }
        else
        {
            elemento = {
                Segundo_paso: 'No existe el elemento con id: ' + idElemento,
                Tercer_paso: 'Fin de la Transaccion'
            }

            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);

                    elemento = 
                    {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:'No existe el elemento con id: ' + idElemento,
                        Tercer_paso: 'Fin de la Transaccion',
                    };
                    res.json({
                        result: elemento
                    });
            });
        }
    })
    

});


app.put('/api/Transacciones/pagarVuelo', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = JSON.stringify(req.body.Segundo_paso).split(" ")[2];

    let reservado = { estado : 'prereservado'};
    let URLVuelo = `${URL_VUELOS}/${idElemento}`;
    let URLPago = `${URL_PAGOS}`;
    const queToken = req.headers.authorization.split(" ")[1];
    

//PAGAMOS LA RESERVA
    fetch(URLPago)
    .then( resp => resp.json() )
    .then(mijson =>
    {

        if(mijson.result == 'Pago realizado correctamente')
        {
            reservado = { estado: 'reservado'};
            fetch(URLVuelo ,{ 
                        method: 'PUT',
                        body: JSON.stringify(reservado),
                        headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                        }
    
            })
            .then( resp => resp.json() )

            
            elemento = {
                Cuarto_paso: 'Pago realizado',
                Quinto_paso: 'Vuelo Reservado'
            };

            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);
                    
                    elemento = {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:req.body.Segundo_paso,
                        Tercer_paso: req.body.Tercer_paso,
                        Cuarto_paso: 'Pago realizado',
                        Quinto_paso: 'Vuelo Reservado'
                    }

                    res.json({
                        result: elemento
                    });
            });
        }
        else
        {
            reservado = { estado: 'libre'};

            fetch(URLVuelo ,{ 
                        method: 'PUT',
                        body: JSON.stringify(reservado),
                        headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}`                                   
                        }
    
            })
            .then( resp => resp.json() )

            
            elemento = {
                Cuarto_paso: 'Error en el pago',
                Quinto_paso: 'Vuelo Liberado'
            };

            coleccion.update(
                {InicioTiempo: req.body.InicioTiempo}, 
                {$set: elemento}, 
                {safe: true, multi: false}, 
                (err, elementoModif) => {
                    if(err) return next(err);
                    
                    elemento = {
                        Primer_Paso: req.body.Primer_Paso,
                        InicioTiempo: req.body.InicioTiempo,
                        Segundo_paso:req.body.Segundo_paso,
                        Tercer_paso: req.body.Tercer_paso,
                        Cuarto_paso: 'Error en el pago',
                        Quinto_paso: 'Vuelo Liberado'
                    }

                    res.json({
                        result: elemento
                    });
            });
        }
    } )
})







//Creamos el Servidor
https.createServer(opciones, app).listen(port, () => {
    console.log(`API GW RESTful CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});






